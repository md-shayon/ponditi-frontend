export interface ClassAndSubjectInterface {
  receiverId: number;
  ClassTypeId?: number;
  SubjectId?: number;
}

export interface SearchFormPropsInterface {
  fromHome: boolean;
}
