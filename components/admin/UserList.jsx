/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/destructuring-assignment */

// React/next
import React from 'react';
import Link from 'next/link';
import Router from 'next/router';

// Redux
import { useAppDispatch } from '../../redux/store';
import { toggleLoading, setErrorList, resetErrorList } from '../../redux/reducers/elementsSlice';
import { fetchAllUsersByAdmin } from '../../redux/reducers/userReducer';

// Config/utils
import axios from '../../config/axios';
import { scheduledclassStatus } from '../../config/keys';

const { APPROVED, REJECTED, PENDING } = scheduledclassStatus;

function UserList(props) {
  const dispatch = useAppDispatch();

  const rejectedUserHandler = async (rue, userId) => {
    rue.preventDefault();
    try {
      dispatch(toggleLoading(true));
      // console.log(userId);
      const response = await axios.put(`/user/reject/${userId}`);
      // console.log(response);
      if (response.status === 202) {
        dispatch(resetErrorList());
        dispatch(fetchAllUsersByAdmin(null));
      }
    } catch (error) {
      console.log(error);
      if (error?.response?.data?.msg) {
        dispatch(setErrorList([error.response.data.msg]));
      }
      if (error?.response?.status === 401 || error?.response?.status === 405) {
        window.localStorage.removeItem('user');
        Router.push('/admin/login');
      }
    } finally {
      // console.log('finally');
      dispatch(toggleLoading(false));
    }
  };

  const acceptUserHandler = async (rue, userId) => {
    rue.preventDefault();
    try {
      dispatch(toggleLoading(true));
      const response = await axios.put(`/user/accept/${userId}`);
      // console.log(response);
      if (response.status === 202) {
        dispatch(resetErrorList());
        // Get all user one more time
        dispatch(fetchAllUsersByAdmin(null));
      }
    } catch (error) {
      console.log(error);
      if (error?.response?.data?.msg) {
        dispatch(setErrorList([error.response.data.msg]));
      }
      if (error?.response?.status === 401 || error?.response?.status === 405) {
        window.localStorage.removeItem('user');
        Router.push('/admin/login');
      }
    } finally {
      // console.log('finally');
      dispatch(toggleLoading(false));
    }
  };

  if (props.allUserList.length > 0) {
    return (
      <tbody className="UserList">
        {props.allUserList.map((aul) => (
          <tr key={aul.id}>
            <td>{aul?.name}</td>
            <td>{aul?.phone}</td>
            <td>{aul?.email}</td>
            <tr>
              <button className=" mx-1 w-fit h-fit bg-transparent border-0" type="button">
                <Link href={`/user/detail/?userId=${aul.id}`}>
                  <img src="/icons/detail.svg" width={20} alt="" />
                </Link>
              </button>
              {aul.isActive === PENDING && (
                <>
                  <button className=" mx-1 w-fit h-fit bg-transparent border-0" type="button" onClick={(e) => acceptUserHandler(e, aul.id)}>
                    <img src="/icons/tick.svg" width={20} alt="" />
                  </button>
                  <button className=" mx-1 bg-transparent border-0" type="button" onClick={(e) => rejectedUserHandler(e, aul.id)}>
                    <img src="/icons/reject.svg" width={20} alt="" />
                  </button>
                </>
              )}

              {aul.isActive === REJECTED && (
                <button className=" mx-1 w-fit h-fit bg-transparent border-0" type="button" onClick={(e) => acceptUserHandler(e, aul.id)}>
                  <img src="/icons/tick.svg" width={20} alt="" />
                </button>
              )}
            </tr>
          </tr>
        ))}
      </tbody>
    );
  }
  return <div className="alert alert-danger w-full">No user found</div>;
}

export default UserList;
