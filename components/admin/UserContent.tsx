/* eslint-disable jsx-a11y/label-has-associated-control */
// React/next
import React, { useState } from 'react';
import Router from 'next/router';

// Redux
import { setSelectedTabElement } from '../../redux/reducers/adminReducer';
import { useAppSelector, useAppDispatch } from '../../redux/store';
import { toggleLoading, setErrorList, resetErrorList } from '../../redux/reducers/elementsSlice';
import { setSearchSingleUser } from '../../redux/reducers/searchReducer';

// Components
import UserList from './UserList';

// Config/utils
import { scheduledclassStatus } from '../../config/keys';
import axios from '../../config/axios';
import Link from 'next/link';

const { APPROVED, REJECTED, PENDING } = scheduledclassStatus;

function UserContent() {
  const dispatch = useAppDispatch();

  const allUserList = useAppSelector((state) => state.user.allUserList);
  const allPendingUserList = useAppSelector((state) => state.user.allPendingUserList);
  const allRejectedUserList = useAppSelector((state) => state.user.allRejectedUserList);
  const allApprovedUserList = useAppSelector((state) => state.user.allApprovedUserList);
  const selectedTabElement = useAppSelector((state) => state.admin.selectedTabElement);
  const adminUserTabElement = useAppSelector((state) => state.admin.adminUserTabElement);
  const searchSingleUser = useAppSelector((state) => state.search.searchSingleUser);

  const [searchParam, setSearchParam] = useState<string>('');

  const changeTabItemHandler = (ctie: React.SyntheticEvent, itemName: string) => {
    ctie.preventDefault();
    // console.log({ selectedTabElement, itemName });
    dispatch(setSelectedTabElement(itemName));
  };

  const showConetent = () => {
    switch (selectedTabElement) {
      case APPROVED: {
        return <UserList allUserList={allApprovedUserList} selectedTabElement={selectedTabElement} />;
      }
      case PENDING: {
        return <UserList allUserList={allPendingUserList} selectedTabElement={selectedTabElement} />;
      }
      case REJECTED: {
        return <UserList allUserList={allRejectedUserList} selectedTabElement={selectedTabElement} />;
      }

      default: {
        return <UserList allUserList={allUserList} selectedTabElement={selectedTabElement} />;
      }
    }
  };

  const searchSingleUserHandler = async (ssue: React.FormEvent<HTMLFormElement>) => {
    ssue.preventDefault();
    if (!searchParam || searchParam === '') return;
    try {
      dispatch(toggleLoading(true));
      const params = {};
      if (searchParam.includes('@')) {
        params.email = searchParam;
      } else if (/^\d+$/.test(searchParam)) {
        params.phone = searchParam;
      } else {
        params.name = searchParam;
      }
      const response = await axios.get(`/search/user`, { params });
      // console.log(response);
      if (response.status === 200) {
        dispatch(resetErrorList());
        // Get all user one more time
        dispatch(setSearchSingleUser(response.data.user));
      }
    } catch (error: any) {
      console.log(error);
      if (error?.response?.data?.msg) {
        dispatch(setErrorList([error.response.data.msg]));
      }
      if (error?.response?.status === 401 || error?.response?.status === 405) {
        window.localStorage.removeItem('user');
        Router.push('/admin/login');
      }
    } finally {
      // console.log('finally');
      dispatch(toggleLoading(false));
    }
  };

  return (
    <div className="UserContent">
      <div className="search-user">
        <h1>Search User</h1>
        <form action="post" onSubmit={searchSingleUserHandler}>
          <div className="input-group mb-3">
            <div className="form-floating">
              <input
                type="text"
                className="form-control"
                defaultValue={searchParam}
                placeholder="Name, phone, or email"
                onChange={(e) => setSearchParam(e.target.value)}
              />
              <label htmlFor="namephoneemail">Name, phone, or email</label>
            </div>
            <span className="input-group-text bg-primary">
              <button type="submit" className="btn btn-primary p-0 m-0 w-full h-full">
                Search
              </button>
            </span>
          </div>
        </form>
        {searchSingleUser.id && (
          <div className="card">
            <h5 className="card-header">{searchSingleUser.name}</h5>
            <div className="card-body">
              {/* <h5 className="card-title">Special title treatment</h5> */}
              <p className="card-text">Phone: {searchSingleUser.phone}</p>
              <p className="card-text">Email: {searchSingleUser.email}</p>
              <Link href={`/user/detail/?userId=${searchSingleUser.id}`} role="button" className="btn btn-primary">
                Detail
              </Link>
            </div>
          </div>
        )}
      </div>
      <div className="user-list">
        <h1>User List</h1>
        <ul className="nav nav-pills bg-danger">
          {adminUserTabElement.map((aut) => (
            <li key={aut.id} className="nav-item">
              <button
                type="button"
                className={aut.name === selectedTabElement ? 'nav-link rounded-1 text-capitalize active' : 'nav-link rounded-1 text-capitalize'}
                onClick={(ctie) => changeTabItemHandler(ctie, aut.name)}
              >
                {aut.text}
              </button>
            </li>
          ))}
        </ul>
        <div className="table-responsive border-top">
          <table className="table">
            <thead className="bg-danger text-white">
              <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Properties</th>
              </tr>
            </thead>
            {showConetent()}
          </table>
        </div>
        {/* {showConetent()} */}
      </div>
    </div>
  );
}

export default UserContent;
