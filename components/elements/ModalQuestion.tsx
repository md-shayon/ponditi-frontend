/* eslint-disable jsx-a11y/control-has-associated-label */
import * as React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { closeModal } from '../../redux/reducers/elementsSlice';
// import useStyles from '../../styles/Home.style.js';
import styles from './modal.module.scss';

import { useAppSelector, useAppDispatch } from '../../redux/store';
import { ModalQuestionPropsIn } from '../../types/components/ElementsInterface';


function ModalQuestion({ modalAnswerHandler, payload }: ModalQuestionPropsIn) {
  // const classes = useStyles();
  const dispatch = useAppDispatch();
  // const [open, setOpen] = React.useState(false);
  const open = useAppSelector((state) => state.elements.modal.open);
  const text = useAppSelector((state) => state.elements.modal.text);
  // const handleOpen = () => setOpen(true);
  // const handleClose = () => setOpen(false);

  // onClose={() => dispatch(closeModal())}
  const modalCloseHandler = (mce: React.SyntheticEvent) => {
    mce.preventDefault();
    dispatch(closeModal());
  };

  const answerModalQuestionHandler = (amqe: React.SyntheticEvent, isAgreed: boolean) => {
    amqe.preventDefault();
    modalAnswerHandler(amqe, isAgreed, payload);
    dispatch(closeModal());
  };
  return (
    <div className={open ? `${styles.modalContainer} d-block` : `${styles.modalContainer} d-none`}>
      <div className={`${styles.customModal} modal d-block`}>
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">{text.heading}</h5>
              <button type="button" className="btn-close" onClick={modalCloseHandler} />
            </div>
            <div className="modal-body">
              <p>{text.body}</p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary" onClick={(e) => answerModalQuestionHandler(e, true)}>
                Yes
              </button>
              <button type="button" className="btn btn-danger" onClick={(e) => answerModalQuestionHandler(e, false)}>
                No
              </button>
              {/* <button type="button" className="btn btn-danger" onClick={(e) => answerModalQuestionHandler(e, false)}>
                Cancel
              </button> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalQuestion;
