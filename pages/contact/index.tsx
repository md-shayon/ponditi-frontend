import React from 'react';
import Layout from '../../components/layouts/Layout';

function Contact() {
  return (
    <Layout title="Contact Us | Ponditi">
      <section className="section section-1">
        <div className="container">Contact</div>
      </section>
    </Layout>
  );
}

export default Contact;
