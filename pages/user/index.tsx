/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/anchor-is-valid */

// React/next
import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSelector, useDispatch } from 'react-redux';

// Components
import Detail from '../../components/user/Detail';
import Layout from '../../components/layouts/Layout';
import Profile from '../../components/user/Profile';
import SOTRequest from '../../components/scheduledclass/SOTRequest';
import ApprovedClass from '../../components/scheduledclass/ApprovedClass';
import RejectedClass from '../../components/scheduledclass/RejectedClass';

// Config/utils
import { userDashboardSidebarList, roles } from '../../config/keys';

// Redux
import { toggleLoading, resetErrorList } from '../../redux/reducers/elementsSlice';
import { setSelectedContent, toggleAuthUser } from '../../redux/reducers/userReducer';
import { fetchAllRequestedSCOU } from '../../redux/reducers/scheduledclassReducer';
import { useAppSelector, useAppDispatch } from '../../redux/store';
import Loader from '@components/elements/Loader';

// Destrecture
const { TEACHER, STUDENT, ADMIN } = roles;
const { CLASS_SCHEDULED, PROFILE, STUDENT_OR_TEACHER_REQUESTS, REJECTED } = userDashboardSidebarList;

function UserIndex() {
  // Hooks
  let isMounted = false;
  const router = useRouter();
  const dispatch = useAppDispatch();

  // State
  const selectedContent = useAppSelector((state) => state.user.selectedContent);
  const authUserInfo = useAppSelector((state) => state.user.authUserInfo);
  const currentUser = useAppSelector((state) => state.user.currentUser);
  const isLoading = useAppSelector((state) => state.elements.isLoading);

  useEffect(() => {
    if (isMounted === false) {
      dispatch(toggleLoading(true));
      const user = localStorage.getItem('user');
      // console.log(user);
      if (user === null) {
        dispatch(toggleAuthUser(false));
        router.push('/user/login');
      } else {
        dispatch(toggleAuthUser(true));
        const userData = JSON.parse(user);
        if (userData.role === ADMIN) {
          router.push('/admin');
        }
      }
      dispatch(resetErrorList());
      dispatch(toggleLoading(false));
    }
    isMounted = true;
  }, []);

  useEffect(() => {
    if (authUserInfo.id) {
      dispatch(fetchAllRequestedSCOU(authUserInfo.id));
    }
  }, [authUserInfo]);

  return (
    <Layout title="User | Ponditi">
      <section className="user-dashboard">
        {isLoading ? (
          <Loader />
        ) : (
          <div className="container">
            <Detail userDetail={currentUser} update search={false} userId={authUserInfo.id} />
          </div>
        )}
      </section>
    </Layout>
  );
}

export default UserIndex;
