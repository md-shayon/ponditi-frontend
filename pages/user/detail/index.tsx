/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable @next/next/no-img-element */

// React/next
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

// Componens
import Layout from '../../../components/layouts/Layout';
import Detail from '../../../components/user/Detail';
import Loader from '../../../components/elements/Loader';

// Redux
import { fetchSelectedSingleUser } from '../../../redux/reducers/userReducer';
import { useAppSelector, useAppDispatch } from '../../../redux/store';

function DetailIndex() {
  let isFetched = false;
  const dispatch = useAppDispatch();
  const router = useRouter();
  const [userId, setUserId] = useState<number>(0);

  const selectedUser = useAppSelector((state) => state.user.selectedUser);
  const isLoading = useAppSelector((state) => state.elements.isLoading);

  useEffect(() => {
    const params = new URLSearchParams(window.location.search);
    const newUserId = params.get('userId');
    // console.log(userId);
    if (newUserId) {
      const userIdInt = parseInt(newUserId, 10);
      setUserId(userIdInt);
      const user = window.localStorage.getItem('user');
      if (isFetched === false && user) {
        (async () => {
          // console.log(userId);
          await dispatch(fetchSelectedSingleUser(userIdInt));
          // await Promise.all([dispatch(fetchSelectedSingleUser(newUserId)), dispatch(fetchCurrentSingleUser(newUserId))]);
        })();
        // Check auth
      }
      isFetched = true;
    } else {
      router.push('/');
    }
  }, []);

  return (
    <Layout title="User Detail | Ponditi">
      <section className="section detail">
        {isLoading ? (
          <Loader />
        ) : (
          <div className="container">
            <Detail userDetail={selectedUser} update={false} search={false} userId={userId} />
          </div>
        )}
      </section>
    </Layout>
  );
}

export default DetailIndex;
