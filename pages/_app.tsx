/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/extensions */
// Typescript
/*
import React from 'react';
import '../styles/globals.scss';
import { AppProps } from 'next/app';

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}
*/

// Javascript
import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { Provider } from 'react-redux';
import { AppProps } from 'next/app';
// import TagManager from 'react-gtm-module';
import store, { useAppDispatch } from '../redux/store';
import '../styles/globals.scss';
import ThemeProvider from '../context/ThemeProvider';
import { toggleLoading } from '../redux/reducers/elementsSlice';

// const tagManagerArgs = {
//   gtmId: process.env.NEXT_PUBLIC_GTM_ID
// }

export default function MyApp({ Component, pageProps }: AppProps) {
  // useEffect(()=>{
  //   TagManager.initialize(tagManagerArgs);
  // }, []);

  const router = useRouter();

  /*
  useEffect(() => {
    const handleRouteChangeOn = (url, { shallow }) => {
      console.log(`routeChangeStart(url, { shallow }) - Fires when a route starts to change --------> App is changing to ${url} ${shallow ? 'with' : 'without'} shallow routing`);
    };

    const handleRouteChangeOff = (url, { shallow }) => {
      console.log(`routeChangeStart(url, { shallow }) - Fires when a route starts to change --------> App is changing to ${url} ${shallow ? 'with' : 'without'} shallow routing`);
    };

    router.events.on('routeChangeStart', handleRouteChangeOn);

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method:
    return () => {
      router.events.off('routeChangeStart', handleRouteChangeOff);
    };
  }, []);
  */
  useEffect(() => {
    // const handleStart = (url: string) => url !== router.asPath && store.dispatch(toggleLoading(true));
    // const handleComplete = (url: string) => url === router.asPath && store.dispatch(toggleLoading(false));

    const runningRoute = (url: string) => {
      // console.log('Running', url);
      store.dispatch(toggleLoading(true));
    };
    const completeRoute = (url: string) => {
      // console.log('Complete', url);
      store.dispatch(toggleLoading(false));
    };

    router.events.on('routeChangeStart', runningRoute);
    router.events.on('routeChangeComplete', completeRoute);
    router.events.on('routeChangeError', completeRoute);

    return () => {
      router.events.off('routeChangeStart', runningRoute);
      router.events.off('routeChangeComplete', completeRoute);
      router.events.off('routeChangeError', completeRoute);
    };
  }, []);

  return (
    <div>
      <Head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <Provider store={store}>
        <ThemeProvider>
          <Component {...pageProps} />
        </ThemeProvider>
      </Provider>
    </div>
  );
}
