import React from 'react';
import Layout from '../../components/layouts/Layout';
import UnderDev from '../../components/elements/UnderDev';

function index() {
  return (
    <Layout title="Privacy Policy | Ponditi">
      <section className="privacy policy container">
        <UnderDev />
      </section>
    </Layout>
  );
}

export default index;
