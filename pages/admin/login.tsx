/* eslint-disable @next/next/no-img-element */

// React/next
import React, { useEffect } from 'react';
import { useRouter } from 'next/router';

// Redux
import { toggleAuthUser } from '../../redux/reducers/userReducer';
import { toggleLoading } from '../../redux/reducers/elementsSlice';
import { useAppSelector, useAppDispatch } from '../../redux/store';

// Component
import Layout from '../../components/layouts/Layout';
import Loader from '../../components/elements/Loader';
import Login from '../../components/admin/Login';

function LoginIndex() {
  const router = useRouter();
  const dispatch = useAppDispatch();

  const isLoading = useAppSelector((state) => state.elements.isLoading);

  // get localstorage
  let isMounted = false;
  useEffect(() => {
    if (isMounted === false) {
      dispatch(toggleLoading(true));
      const user = localStorage.getItem('user');
      // console.log(user);
      if (user === null) {
        dispatch(toggleAuthUser(false));
      } else {
        dispatch(toggleAuthUser(true));
        router.push('/admin');
      }
      dispatch(toggleLoading(false));
    }
    isMounted = true;
  }, []);

  return (
    <Layout title="Admin Login | Ponditi">
      {isLoading ? (
        <Loader />
      ) : (
        <section className="section-1 Admin-login">
          <div className="container">
            <div className="row mt-5">
              <div className="lockicon col-md-6 d-none d-md-block">
                <img src="/shape/login.svg" alt="" className="img-fluid" />
              </div>
              <div className="login-component col-md-6">
                <Login />
              </div>
            </div>
          </div>
        </section>
      )}
    </Layout>
  );
}

export default LoginIndex;
