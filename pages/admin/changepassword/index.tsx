// React/next
import React, { useEffect } from 'react';
import Router from 'next/router';

// Redux
import { useAppSelector, useAppDispatch } from '../../../redux/store';
import { toggleLoading, resetErrorList } from '../../../redux/reducers/elementsSlice';
import { toggleAuthUser } from '../../../redux/reducers/userReducer';

// Components
import Layout from '../../../components/layouts/Layout';
import ChangePass from '../../../components/changepassword/ChangePass';
import Loader from '../../../components/elements/Loader';
import MessageList from '../../../components/elements/MessageList';

function ChangepasswordIndex() {
  let isMounted = false;
  const dispatch = useAppDispatch();

  const isLoading = useAppSelector((state) => state.elements.isLoading);

  useEffect(() => {
    if (isMounted === false) {
      dispatch(toggleLoading(true));
      const user = localStorage.getItem('user');
      if (user === null) {
        dispatch(toggleAuthUser(false));
        Router.push('/user/login');
      } else {
        dispatch(toggleAuthUser(true));
      }
      dispatch(resetErrorList([]));
      dispatch(toggleLoading(false));
    }
    isMounted = true;
  }, []);

  return (
    <Layout title="Admin Password Change | Ponditi">
      {isLoading ? (
        <Loader />
      ) : (
        <div className="changepassword">
          <MessageList />
          <ChangePass />
        </div>
      )}
    </Layout>
  );
}

export default ChangepasswordIndex;
