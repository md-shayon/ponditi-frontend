// React/next
import React, { useEffect } from 'react';
import { useRouter } from 'next/router';

// Components
import Layout from '../../components/layouts/Layout';
import Loader from '../../components/elements/Loader';
import Dashboard from '../../components/admin/Dashboard';

// Redux
import { useAppSelector, useAppDispatch } from '../../redux/store';
import { toggleAuthUser, fetchAllUsersByAdmin, resetAllUserList } from '../../redux/reducers/userReducer';
import { toggleLoading } from '../../redux/reducers/elementsSlice';
import { fetchAllClassTypes } from '../../redux/reducers/classtypeReducer';
import { fetchAllSubjects } from '../../redux/reducers/subjectReducer';
import { fetchAllTuitionms } from '../../redux/reducers/tuitionmReducer';

// Config/utils
import { roles } from '../../config/keys';

// Styles
import styles from '../../styles/modules/Admin.module.scss'

const { ADMIN } = roles;

function AdminIndex() {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const isLoading = useAppSelector((state) => state.elements.isLoading);

  // get localstorage
  let isMounted = false;
  useEffect(() => {
    if (isMounted === false) {
      dispatch(toggleLoading(true));
      const user = window.localStorage.getItem('user');
      const userData = JSON.parse(user);
      if (userData && userData.role === ADMIN) {
        dispatch(toggleAuthUser(true));
        dispatch(toggleLoading(false));

        // Fetch data
        (async () => {
          await Promise.all([dispatch(fetchAllUsersByAdmin()), dispatch(fetchAllClassTypes(null)), dispatch(fetchAllSubjects(null)), dispatch(fetchAllTuitionms(null))]);
        })();
      } else {
        dispatch(toggleAuthUser(false));
        window.localStorage.removeItem('user');
        router.push('/admin/login');
      }
    }
    isMounted = true;
    // return () => {
    //   dispatch(resetAllUserList());
    // };
  }, []);

  // otherwise use dashboard component
  return (
    <Layout title="Admin Panel | Ponditi">
      {isLoading ? (
        <Loader />
      ) : (
        <section className={`section-1 ${styles.Admin}`}>
          <Dashboard styles={styles} />
        </section>
      )}
    </Layout>
  );
}

export default AdminIndex;
